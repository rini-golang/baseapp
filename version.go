package app

import (
	"os"

	"github.com/rs/zerolog/log"
)

type Version bool

// func (v Version) Decode(ctx *kong.DecodeContext) error { return nil }
// func (v Version) IsBool() bool                         { return true }

func (v Version) AfterApply() error {
	log.Info().
		Str("version", buildVersion).
		Str("date", buildDate).
		Str("commit", buildCommit).
		Msg("")

	if v {
		os.Exit(0)
	}

	return nil
}

var (
	buildVersion = "N/A"
	buildDate    = "N/A"
	buildCommit  = "N/A"
)
