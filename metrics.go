package app

import (
	"context"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog/log"
)

// StartMetrics .
func (app *App) startMetrics(ctx context.Context, wg *sync.WaitGroup) {
	onStop := StartStopFunc(ctx, wg)

	defer onStop()

	app.isHealthy.Store(false)
	app.isReady.Store(false)

	handler := http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
		if req.Method == "GET" {
			switch req.URL.Path {
			case "/readyz":
				if app.isReady == nil || !app.isReady.Load().(bool) {
					resp.WriteHeader(http.StatusServiceUnavailable)
					return
				}
				resp.WriteHeader(http.StatusOK)
				return
			case "/healthz":
				if app.isHealthy == nil || !app.isHealthy.Load().(bool) {
					resp.WriteHeader(http.StatusServiceUnavailable)
					return
				}
				resp.WriteHeader(http.StatusOK)
				return
			}
		}
		// log.Info().Msg("1233")
		promhttp.HandlerFor(app.metrics, promhttp.HandlerOpts{}).ServeHTTP(resp, req)
		// promhttp.InstrumentMetricHandler(app.metrics, promhttp.HandlerFor(prometheus.DefaultGatherer, promhttp.HandlerOpts{})).ServeHTTP(resp, req)
	})
	h := &http.Server{
		Addr:              metricPort(),
		Handler:           handler,
		ReadHeaderTimeout: 60 * time.Second,
	}
	ctxDead, cancel := context.WithTimeout(context.Background(), 2*time.Second)

	go func() {
		<-app.ctx.Done()
		defer cancel()

		if err := h.Shutdown(ctxDead); err != nil {
			log.Err(err)
		}
	}()

	err := h.ListenAndServe()
	if err != nil {
		if err.Error() != "http: Server closed" {
			log.Error().Err(err).Msg("start metrics failed")
			return
		}
	}

	// cancel()
	// fmt.Println("stoped")
}

type Metrics bool

func (d Metrics) AfterApply(app *App) error {
	if d {
		app.StartMetrics(app.AddJob("metrics"), app.wg)
	}

	return nil
}

func (app *App) SetReady(r bool) {
	app.isReady.Store(r)
}

func (app *App) SetHealthy(h bool) {
	app.isHealthy.Store(h)
}
func metricPort() string {
	port := os.Getenv("METRICS")
	if len(port) != 0 && strings.ContainsRune(port, ':') {
		return port
	}

	return ":9100"
}

func (app *App) StartMetrics(ctx context.Context, wg *sync.WaitGroup) {
	appMetric := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "app",
		Help: "Application name",
		ConstLabels: prometheus.Labels{
			"name": app.name,
		}},
	)

	appMetric.Set(1)

	app.jobMetric = *prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "job",
		Help: "Job name",
	}, []string{"name"},
	)

	app.metrics.MustRegister(appMetric, app.jobMetric, collectors.NewGoCollector())
	wg.Add(1)
	go app.startMetrics(ctx, wg)
}
