package app

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/creasty/defaults"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

var APP *App

type Globals struct {
	App     `kong:"-"`
	Config  string  `help:"Location of config file" default:"config.yaml" type:"path"`
	Debug   Debug   `short:"v" help:"Enable debug mode"`
	Version Version `help:"Print version information and quit"`
	Metrics Metrics `help:"Expose prometheus metrics" neogatable:"" default:"true"`
}
type App struct {
	isHealthy   *atomic.Value
	isReady     *atomic.Value
	metrics     *prometheus.Registry
	wg          *sync.WaitGroup
	Jobs        *sync.Map
	jobMetric   prometheus.GaugeVec
	ctx         context.Context
	cancelCtx   context.CancelFunc
	name        string
	description string
}

// RegisterMetric .
func (app *App) RegisterMetric(cs ...prometheus.Collector) {
	app.metrics.MustRegister(cs...)
}

// AddJob .
func (app *App) AddJob(name string) context.Context {
	return context.WithValue(app.ctx, ContextKeyJobName, name)
}

// WG .
func (app *App) WG() *sync.WaitGroup {
	return app.wg
}

func NewApp(name, desc string) *App {
	log.Logger = log.Logger.
		Level(zerolog.InfoLevel).
		With().Caller().Logger()

	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}

	signal.Notify(terminate, os.Interrupt, syscall.SIGTERM)

	// go func() {
	// 	<-terminate

	// 	waitDone := make(chan struct{})

	// 	go func() {
	// 		wg.Wait()
	// 		waitDone <- struct{}{}
	// 	}()
	// 	cancel()
	// 	select {
	// 	case <-waitDone:
	// 		os.Exit(0)
	// 	case <-time.NewTimer(5 * time.Second).C:
	// 		log.Error().Msg("Program exit by timeout")
	// 		os.Exit(1)
	// 	}
	// }()
	go func() {
		<-terminate
		cancel()

		go func() {
			time.Sleep(15 * time.Second)
			log.Error().Msg("Program exit by timeout")
			os.Exit(1)
		}()
	}()

	return &App{
		name:        name,
		description: desc,
		ctx:         ctx,
		cancelCtx:   cancel,
		Jobs:        &sync.Map{},
		isHealthy:   new(atomic.Value),
		isReady:     new(atomic.Value),
		metrics:     prometheus.NewRegistry(),
		wg:          wg,
	}
}

func (app *App) Cancel() {
	app.cancelCtx()
}

type CmdConfig interface {
	ConfigPath() string
	GetConfig() interface{}
}

// StartStopFunc .
func StartStopFunc(ctx context.Context, wg *sync.WaitGroup) func() {
	// wg.Add(1)

	job, ok := ctx.Value(ContextKeyJobName).(string)
	if !ok {
		return func() {
			wg.Done()
		}
	}

	if len(job) == 0 {
		panic(fmt.Sprintf("null job name: %s", job))
	}

	if APP != nil {
		_, ok := APP.Jobs.Load(job)
		if ok {
			panic(fmt.Sprintf("dublicate job name: %s", job))
		}

		if APP.jobMetric.MetricVec != nil {
			APP.jobMetric.With(prometheus.Labels{"name": job}).Set(1)
		}

		APP.Jobs.Store(job, struct{}{})
	}

	if len(job) != 0 {
		log.Info().Msg(job + " started")
	}

	return func() {
		if APP != nil {
			if APP.jobMetric.MetricVec != nil {
				APP.jobMetric.Delete(prometheus.Labels{"name": job})
			}

			APP.Jobs.Delete(job)
		}

		wg.Done()
		log.Info().Msg(job + " stoped")
	}
}

func ReadFromFile(filename string, out interface{}) error {
	if len(filename) == 0 {
		return nil
	}

	if _, err := os.Stat(filename); errors.Is(err, os.ErrNotExist) {
		yamlData, err := yaml.Marshal(out)
		if err != nil {
			return err
		}

		err = os.WriteFile(filename, yamlData, 0600)
		if err != nil {
			return err
		}
	}

	if err := defaults.Set(out); err != nil {
		return err
	}

	viper.AutomaticEnv()
	viper.SetConfigFile(filename)

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	if err := viper.Unmarshal(out); err != nil {
		log.Error().Err(err).Msg("")
		return err
	}

	return nil
}

var (
	// ContextKeyJobName .
	ContextKeyJobName = "jobName"
	terminate         = make(chan os.Signal, 1)
)

func GlobalCancel() {
	terminate <- os.Signal(syscall.SIGTERM)
}

func SetReady(r bool) {
	APP.isReady.Store(r)
}

func SetHealthy(h bool) {
	APP.isHealthy.Store(h)
}