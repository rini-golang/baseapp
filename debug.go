package app

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type Debug bool

func (d Debug) BeforeApply() error {
	zerolog.SetGlobalLevel(zerolog.DebugLevel)

	log.Logger = log.Logger.
		Level(zerolog.DebugLevel).
		Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: "2006-01-02 15:04:05"}).
		With().Caller().Logger()

	return nil
}
